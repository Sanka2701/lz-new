export const LOCALE_CHANGED = 'LOCALE_CHANGED';
export const AUTH_USER = 'AUTH_USER';
export const AUTH_USER_OUT = 'AUTH_USER_OUT';
export const AUTH_ERROR = 'AUTH_ERROR';
export const GOOGLE_PLACE_SELECTED = 'GOOGLE_PLACE_SELECTED';
export const PLACE_SELECTED = 'PLACE_SELECTED';
export const PLACES_RECEIVED = 'PLACES_RECEIVED';